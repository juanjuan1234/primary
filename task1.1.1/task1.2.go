package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"time"
)

var (
	anslist [10000]string
	length  int
	flag    bool
	i       int
)

func ScanWithCancel(ctx context.Context) {
	var ans string
LOOP:
	for {
		select {
		case <-ctx.Done():
			break LOOP
		default:
			//fmt.Scanf("%d", &ans)  无法读入
			fmt.Scan(&ans)
			anslist[i] = ans
			flag = true
			break LOOP
		}
	}
}

func main() {
	//文件定向
	var correctans = 0
	fileName := "problems.csv"
	fs, err := os.Open(fileName)
	if err != nil {
		log.Fatalf("Can not open the file, err is %+v", err)
	}
	defer fs.Close()
	reader := csv.NewReader(fs)
	var data [][]string
	preData, err := reader.ReadAll()
	data = preData
	length = len(preData) - 1
	//处理
	var waittime int
	fmt.Println("please input wait time for each question:")
	fmt.Scan(&waittime)
	for i = 0; i <= length; i++ {
		fmt.Printf("question%d:  %s\n", i+1, data[i][0])
		ctx, canf := context.WithCancel(context.Background())
		go ScanWithCancel(ctx)
		for k := 1; flag == false && k <= waittime; k++ {
			time.Sleep(time.Second)
		}
		flag = false
		canf()
		if data[i][1] == anslist[i] {
			correctans++
		}
	}
	fmt.Printf("total question=%d    correct ones=%d", length+1, correctans)
}
