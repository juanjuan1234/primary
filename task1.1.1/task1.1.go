package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
)

func main() {
	//文件定向
	var correctans = 0
	fileName := "problems.csv"
	fs, err := os.Open(fileName)
	if err != nil {
		log.Fatalf("Can not open the file, err is %+v", err)
	}
	defer fs.Close()
	reader := csv.NewReader(fs)
	var data [][]string
	preData, err := reader.ReadAll()
	data = preData
	length := len(preData) - 1
	var ans string
	for i := 0; i <= length; i++ {
		println(data[i][0])
		fmt.Scan(&ans)
		if ans == data[i][1] {
			correctans = correctans + 1
		}
	}
	fmt.Printf("%d    %d", length+1, correctans)
}
