#include<WinSock2.h>
#include<stdio.h>
#include<Windows.h>
SOCKET tsock;
#define SERVER_PORT 10000
#define SERVERIP "127.0.0.1"
#define BUFSIZE 4096
char *buffer="Hello!";
void InitSocket(){
    int ret;
    WORD sockVersion=MAKEWORD(2,2);
    WSADATA wsaDATA;
    ret=WSAStartup(sockVersion,&wsaDATA);
    if(ret!=0){
        printf("Couldn't find a useable winsock.dll!\n");
        exit(-1);
    }
}
void CreateSocket(){
    tsock=socket(AF_INET,SOCK_DGRAM,0);
    if(tsock==INVALID_SOCKET){
        printf("Create socket failed!\n");
        exit(-1);
    }
}
void SendAndRecv(){
    struct sockaddr_in servaddr;
    servaddr.sin_family=AF_INET;
    //inet_aton(SERVERIP,&servaddr.sin_addr);
    servaddr.sin_addr.s_addr=inet_addr(SERVERIP);
    servaddr.sin_port=htons(SERVER_PORT);
    int num=0;
    int len=sizeof(struct sockaddr_in);
    num=sendto(tsock,buffer, strlen(buffer),0,(struct sockaddr*)&servaddr,len);
    if(num!=strlen(buffer)){
        printf("Send data failed!\n");
        exit(-1);
    }
    char buf[BUFSIZE];
    num=0;
    struct sockaddr_in clientaddr;
    ZeroMemory(&clientaddr,sizeof(clientaddr));
    ZeroMemory(buf,sizeof(buf));
    num=recvfrom(tsock,buf,sizeof(buf),0,(struct sockaddr*)&clientaddr,&len);
    if(num<0){
        printf("Receive data failed!\n");
        exit(-1);
    }
}
void CloseSocket(){
    closesocket(tsock);
    WSACleanup();
}
int main(){
    InitSocket();
    CreateSocket();
    while(1){
        SendAndRecv();
        Sleep(500);
    }
    
    CloseSocket();
    return 0;
}
