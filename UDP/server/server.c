#include<stdio.h>
#include<WinSock2.h>
#include<Windows.h>
#include<stdbool.h>                        //netstat -ano | findstr 10000
#define PORT 10000                         // taskkill /f /pid 242508
#define BUFSIZE 4096
#define SERVERIP "127.0.0.1"
#pragma comment(lib,"ws2_32")              //但这招在VsCode中不适用
SOCKET msock;
char buf[BUFSIZE];
void InitSocket();
void ApplyCon();
void RecvAndSend();
void CloseSocket();
int main(){
    InitSocket();
    msock=socket(AF_INET,SOCK_DGRAM,0);
    if(msock==INVALID_SOCKET){
        printf("Create socket falied.\n");
        exit(-1);
    }
    ApplyCon();
    while(1){
        RecvAndSend();
    }
    
   
    CloseSocket();
    return 0;
}
void InitSocket(){
    /*LPWSADATA lpWSAData;
    if(0!=WSAStartup(MAKEWORD(2,2),lpWSAData)){
        printf("Initing failed: %d\n",WSAGetLastError());
        return false;
    }
    return true;*/
    int ret;
    WORD sockVersion=MAKEWORD(2,2);
    WSADATA wsaData;
    ret=WSAStartup(sockVersion,&wsaData);
    if(ret!=0){
        printf("Couldn't find a useable winsock.dll\n");
        exit(-1);
    }
}
void CloseSocket(){
    closesocket(msock);
    WSACleanup();
}
void ApplyCon(){
    int ret;
    struct sockaddr_in serAddr;
    ZeroMemory(&serAddr,sizeof(serAddr));
    serAddr.sin_family = AF_INET;     
    serAddr.sin_port = htons(PORT);   
    serAddr.sin_addr.s_addr=inet_addr(SERVERIP);
    //为什么不可行serAddr.sin_addr.s_addr=inet_addr(INADDR_ANY);
    ret=bind(msock,(struct sockaddr*)&serAddr,sizeof(serAddr));  
    if(ret<0){
        printf("Server bind port:% d failed!\n",PORT);
        exit(-1);
    }
     /*int times = 0;
     while(times < 1000000 && SOCKET_ERROR == bind(msock, (SOCKADDR *)&serAddr, sizeof(serAddr)))

    { 
          printf("CanThread: Bind socket error! Reason %d\n", WSAGetLastError());
          times++;
          Sleep(200);

   }*/

}
void RecvAndSend(){
    int num=0;
    struct sockaddr_in clientaddr;
    int len=sizeof(clientaddr);
    ZeroMemory(&clientaddr,sizeof(clientaddr));
    ZeroMemory(buf,sizeof(buf));
    num=recvfrom(msock,buf,sizeof(buf),0,(struct sockaddr*)&clientaddr,&len);
    if(num<0){
        printf("Receive data failed!\n");
        exit(-1);
    }
    printf("%s",buf);
    num=sendto(msock,buf,strlen(buf),0,(struct sockaddr*)&clientaddr,len);
    if(num!=strlen(buf)){
        printf("Send Data failed!\n");
        exit(-1);
    }
}

